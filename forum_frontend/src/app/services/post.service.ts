import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment as env } from '../../environments/environment';
import { ApiPost, CreatePost, Post } from '../models/post.model';
import { map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private http: HttpClient) { }

  createPost(postData: CreatePost) {
    const formData = new FormData();

    Object.keys(postData).forEach(key => {
      if (postData[key] !== null) {
        formData.append(key, postData[key]);
      }
    });

    return this.http.post<Post>(env.apiUrl + '/posts', formData, {
      headers: new HttpHeaders({'Authorization': postData.token})
    });
  }

  getPosts() {
    return this.http.get<ApiPost[]>(env.apiUrl + '/posts').pipe( map( response => {
      return response.map( postData => {
        return new Post(
          postData._id,
          postData.title,
          postData.user,
          postData.description,
          postData.datetime,
          postData.image
        )});
    }))
  }

  getOnePost(id: string) {
    return this.http.get<ApiPost>(env.apiUrl + '/posts/' + id).pipe(
      map( response => {
        return response;
      })
    )
  }
}
