import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiComment, CreateComment, Comment } from '../models/comment.model';
import { environment as env } from '../../environments/environment';
import { map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommentService {
  constructor(private http: HttpClient) {}

  createComment(commentData: CreateComment) {
    return this.http.post<Comment>(env.apiUrl + '/comments', commentData, {
      headers: new HttpHeaders({'Authorization': commentData.token})
    });
  }

  getComments(id: string) {
    return this.http.get<Comment[]>(env.apiUrl + '/comments/' + id).pipe( map( response => {
      return response.map( commentData => {
        return new Comment(
          commentData._id,
          commentData.user,
          commentData.post,
          commentData.description,
        )});
    }))
  }

}
