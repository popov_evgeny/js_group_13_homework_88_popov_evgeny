import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { PostsComponent } from './pages/posts/posts.component';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { PostComponent } from './pages/post/post.component';
import { RegisterFormComponent } from './pages/register-form/register-form.component';
import { LoginFormComponent } from './pages/login-form/login-form.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormCardComponent } from './ui/form-card/form-card.component';
import { ActionReducer, MetaReducer, StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { postReducer } from './store/post.reducer';
import { PostEffects } from './store/post.effects';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { UsersEffects } from './store/users.effects';
import { usersReducer } from './store/users.reducer';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { PostFormComponent } from './pages/post-form/post-form.component';
import { localStorageSync } from 'ngrx-store-localstorage';
import { FileInputComponent } from './ui/file-input/file-input.component';
import { CommentReducer } from './store/comment.reducer';
import { CommentEffects } from './store/comment.effects';
import { ValidatePasswordDirective } from './services/validate-password.directive';

export const localStorageSyncReducer = (reducer: ActionReducer<any>) => {
  return localStorageSync({
    keys: [{'user': ['user']}],
    rehydrate: true
  })(reducer);
}

const metaReducers: Array<MetaReducer> = [localStorageSyncReducer];

@NgModule({
    declarations: [
        AppComponent,
        PostsComponent,
        PostComponent,
        RegisterFormComponent,
        LoginFormComponent,
        FormCardComponent,
        PostFormComponent,
        FileInputComponent,
        ValidatePasswordDirective
    ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    MatToolbarModule,
    MatButtonToggleModule,
    MatCardModule,
    MatIconModule,
    MatMenuModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBarModule,
    StoreModule.forRoot({
      posts: postReducer,
      user: usersReducer,
      comments: CommentReducer
    }, {metaReducers}),
    EffectsModule.forRoot([PostEffects, UsersEffects, CommentEffects]),
    MatProgressSpinnerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
