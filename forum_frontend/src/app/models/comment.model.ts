export interface ApiComment {
  _id: string,
  user: {
    _id: string,
    name: string
  },
  post: {
    _id: string,
    title: string
  },
  description: string
}

export class Comment {
  constructor(
    public _id: string,
    public user: {
      _id: string,
      name: string
    },
    public post: {
      _id: string,
      title: string
    },
    public description: string,
  ) {}
}

export interface CreateComment {
  [key: string]: any;
  post: string,
  token: string
  description: string,
}

export interface FieldError {
  message: string
}

export interface CreateError {
  errors: {
    description: FieldError,
    image: FieldError,
  }
}
