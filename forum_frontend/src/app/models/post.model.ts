export interface ApiPost {
  _id: string,
  title: string,
  user: {
    _id: string,
    name: string
  },
  description: string
  datetime: string,
  image: File | null
}

export class Post {
  constructor(
    public _id: string,
    public title: string,
    public user: {
      _id: string,
      name: string
    },
    public description: string,
    public datetime: string,
    public image: File | null
  ) {}
}

export interface CreatePost {
  [key: string]: any;
  title: string,
  description: string,
  image: File | null,
  token: string
}

