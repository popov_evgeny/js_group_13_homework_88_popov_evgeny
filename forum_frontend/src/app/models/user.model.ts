export class User {
  constructor(
    public _id: string,
    public email: string,
    public name: string,
    public token: string
  ) {}
}

export interface ApiUser {
  email: string,
  password: string,
  name: string
}

export interface FieldError {
  message: string
}

export interface RegisterError {
  errors: {
    email: FieldError,
    password: FieldError,
    name: FieldError
  }
}

export interface LoginUserData {
  email: string,
  password: string,
}

export interface LoginError {
  error: string
}
