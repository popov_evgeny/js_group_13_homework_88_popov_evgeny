import { AfterViewInit, Component, OnDestroy, ViewChild } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { ApiUser, RegisterError } from '../../models/user.model';
import { NgForm } from '@angular/forms';
import { registerRequest } from '../../store/users.actions';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.sass']
})
export class RegisterFormComponent implements AfterViewInit, OnDestroy{
  @ViewChild('form') form!: NgForm;
  loading: Observable<boolean>;
  error: Observable<null | RegisterError>;
  errSubscription!: Subscription;


  constructor(private store: Store<AppState>) {
    this.loading = store.select( state => state.user.registerLoading);
    this.error = store.select( state => state.user.registerError);
  }

  ngAfterViewInit(): void {
    this.errSubscription = this.error.subscribe(error => {
      if (error) {
        const message = error.errors.email.message;
        this.form.form.get('email')?.setErrors({serverError: message});
      } else {
        this.form.form.get('email')?.setErrors({});
      }
    });
  }

  onSubmit() {
    const userData: ApiUser = {
      email: this.form.value.email,
      password: this.form.value.password,
      name: this.form.value.name
    }
    this.store.dispatch(registerRequest({userData}));
  }

  ngOnDestroy() {
    this.errSubscription.unsubscribe();
  }
}
