import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { createPostRequest } from '../../store/post.actions';
import { NgForm } from '@angular/forms';
import { CreatePost } from '../../models/post.model';
import { Observable, Subscription } from 'rxjs';
import { User } from '../../models/user.model';
import { CreateError } from '../../models/comment.model';

@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.sass']
})
export class PostFormComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('form') form!: NgForm;
  user: Observable<null | User>;
  userSubscription!: Subscription;
  error: Observable<null | CreateError>;
  errSubscription!: Subscription;
  loading: Observable<boolean>;
  token!: string;

  constructor(private store: Store<AppState>) {
    this.user = store.select( state => state.user.user);
    this.error = store.select(state => state.posts.createError);
    this.loading = store.select(state => state.posts.createLoading);
  }

  ngOnInit(): void {
    this.userSubscription = this.user.subscribe( user => {
      if (user?.token) {
        this.token = user.token;
      }
    });
  }

  ngAfterViewInit(): void {
    this.errSubscription = this.error.subscribe(error => {
      if (error?.errors.description.message === '') {
        const message = error.errors.image.message;
        this.form.form.get('image')?.setErrors({serverError: message});
      } else {
        this.form.form.get('image')?.setErrors({});
      }
      if (error?.errors.image.message === '') {

        const message = error.errors.description.message;
        this.form.form.get('description')?.setErrors({serverError: message});
      } else {
        this.form.form.get('description')?.setErrors({});
      }
    });  // я так и не понял как правильно написать эту проверку
  }

  onSubmit() {
    const post: CreatePost = this.form.value;
    post.token = this.token;
    this.store.dispatch(createPostRequest({post}));
  }
  ngOnDestroy() {
    this.errSubscription.unsubscribe();
  }
}
