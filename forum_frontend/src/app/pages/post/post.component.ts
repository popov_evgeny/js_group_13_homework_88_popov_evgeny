import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { createPostRequest, fetchPostRequest } from '../../store/post.actions';
import { Observable, Subscription } from 'rxjs';
import { CreatePost, Post } from '../../models/post.model';
import { environment } from '../../../environments/environment';
import { NgForm } from '@angular/forms';
import { createCommentRequest, fetchCommentsRequest } from '../../store/comment.actions';
import { ApiComment, CreateComment } from '../../models/comment.model';
import { User } from '../../models/user.model';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.sass']
})
export class PostComponent implements OnInit, OnDestroy {
  @ViewChild('form') form!: NgForm;
  user: Observable<null | User>;
  userSubscription!: Subscription;
  post: Observable<Post | null>;
  postSubscription!: Subscription;
  loading: Observable<boolean>;
  error: Observable<null | string>;
  apiUrl = environment.apiUrl
  postInformation!: Post;
  comments: Observable<ApiComment[]>;
  errorComment: Observable<null | string>;
  loadingComment: Observable<boolean>;
  postId!: string;
  token!: string;

  constructor(
    private route: ActivatedRoute,
    private store: Store<AppState>
  ) {
    this.user = store.select( state => state.user.user);
    this.post = store.select( state => state.posts.post);
    this.loading = store.select( state => state.posts.fetchLoading);
    this.error = store.select( state => state.posts.fetchError);
    this.comments = store.select( state => state.comments.comments);
    this.loadingComment = store.select( state => state.comments.fetchLoading);
    this.errorComment = store.select( state => state.comments.fetchError);
  }

  ngOnInit(): void {
    this.userSubscription = this.user.subscribe( user => {
      if (user?.token) {
        this.token = user.token;
      }
    });
    this.route.params.subscribe(params => {
      this.postId = params['id'];
      this.store.dispatch(fetchPostRequest({id: params['id']}))
    });
    this.postSubscription = this.post.subscribe( post => {
      this.postInformation = <Post>post
    })
    this.store.dispatch(fetchCommentsRequest({id: this.postId}));
  }

  onSubmit() {
    const comment: CreateComment = this.form.value;
    comment.token = this.token;
    comment.post = this.postId;
    this.store.dispatch(createCommentRequest({comment}));
    this.form.resetForm();
    this.store.dispatch(fetchCommentsRequest({id: this.postId}));
  }

  ngOnDestroy() {
    this.postSubscription.unsubscribe();
  }
}
