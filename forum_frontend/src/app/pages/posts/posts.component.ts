import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Post } from '../../models/post.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { fetchPostsRequest } from '../../store/post.actions';
import { environment as env } from '../../../environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.sass']
})
export class PostsComponent implements OnInit {
  apiUrl = env.apiUrl;
  posts: Observable<Post[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>;


  constructor(private store: Store<AppState>, private router: Router,) {
    this.posts = store.select( state => state.posts.posts);
    this.loading = store.select( state => state.posts.fetchLoading);
    this.error = store.select( state => state.posts.fetchError);
  }

  ngOnInit() {
    this.store.dispatch(fetchPostsRequest());
  }


  onClickCard(_id: string) {
    void this.router.navigate(['post', _id, 'information']);
  }
}
