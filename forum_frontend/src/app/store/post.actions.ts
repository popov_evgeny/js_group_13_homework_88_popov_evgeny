import { createAction, props } from '@ngrx/store';
import { ApiPost, CreatePost, Post } from '../models/post.model';
import { CreateError } from '../models/comment.model';

export const fetchPostsRequest = createAction('[Post] Fetch Request');
export const fetchPostsSuccess = createAction('[Post] Fetch Success', props<{posts: Post[]}>());
export const fetchPostsFailure = createAction('[Post] Fetch Failure', props<{error: string}>());

export const fetchPostRequest = createAction('[Post] FetchById Request', props<{id: string}>());
export const fetchPostSuccess = createAction('[Post] FetchById Success', props<{post: ApiPost}>());
export const fetchPostFailure = createAction('[Post] FetchById Failure', props<{error: string}>());

export const createPostRequest = createAction('[Post] Create Request', props<{post: CreatePost}>());
export const createPostSuccess = createAction('[Post] Create Success');
export const createPostFailure = createAction('[Post] Create Failure', props<{error: null | CreateError}>());
