import { createAction, props } from '@ngrx/store';
import { ApiComment, Comment, CreateComment } from '../models/comment.model';

export const fetchCommentsRequest = createAction('[Comments] Fetch Request', props<{id: string}>());
export const fetchCommentsSuccess = createAction('[Comments] Fetch Success', props<{comments: ApiComment[]}>());
export const fetchCommentsFailure = createAction('[Comments] Fetch Failure', props<{error: string}>());

export const createCommentRequest = createAction('[Comment] Create Request', props<{comment: CreateComment}>());
export const createCommentSuccess = createAction('[Comment] Create Success');
export const createCommentFailure = createAction('[Comment] Create Failure', props<{error: string}>());
