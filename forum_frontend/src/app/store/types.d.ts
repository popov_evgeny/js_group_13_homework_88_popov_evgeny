import { Post } from '../models/post.model';
import { LoginError, RegisterError, User } from '../models/user.model';
import { ApiComment, CreateError } from '../models/comment.model';

export type PostState = {
  posts: Post[],
  post: Post | null,
  fetchLoading: boolean,
  fetchError: null | string,
  createError: null | CreateError,
  createLoading: boolean
};

export type CommentState = {
  comments: ApiComment[],
  comment: Comment | null,
  fetchLoading: boolean,
  fetchError: null | string,
  createError: null | string
  createLoading: boolean
};

export type UserState = {
  user: null | User,
  registerLoading: boolean,
  registerError: null | RegisterError,
  loginLoading: boolean,
  loginError: null | LoginError

};

export type AppState = {
  posts: PostState,
  user: UserState,
  comments: CommentState
}
