import { createReducer, on } from '@ngrx/store';
import { UserState } from './types';
import {
  loginFailure,
  loginRequest,
  loginSuccess, logoutUser,
  registerFailure,
  registerRequest,
  registerSuccess
} from './users.actions';


const initialState : UserState = {
  user: null,
  registerLoading: false,
  registerError: null,
  loginLoading: false,
  loginError: null
}

export const usersReducer = createReducer(
  initialState,
  on(registerRequest, state => ({...state, registerLoading: true, registerError: null})),
  on(registerSuccess, (state, {user}) => ({...state, registerLoading: false, user})),
  on(registerFailure, (state, {error}) => ({...state, registerLoading: false, registerError: error})),

  on(loginRequest, state => ({...state, loginLoading: true, loginError: null,})),
  on(loginSuccess, (state, {user}) => ({...state, loginLoading: false, user})),
  on(loginFailure, (state, {error}) => ({...state, loginLoading: false, loginError: error})),
  on(logoutUser, state => ({...state, user: null,}))
)
