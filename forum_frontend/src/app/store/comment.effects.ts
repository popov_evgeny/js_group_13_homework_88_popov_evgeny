import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, mergeMap, of } from 'rxjs';
import {
  createCommentFailure,
  createCommentRequest,
  createCommentSuccess,
  fetchCommentsFailure,
  fetchCommentsRequest,
  fetchCommentsSuccess
} from './comment.actions';
import { CommentService } from '../services/comment.service';

@Injectable()

export class CommentEffects {
  fetchComments = createEffect(() => this.actions.pipe(
    ofType(fetchCommentsRequest),
    mergeMap( id => this.commentService.getComments(id.id).pipe(
      map( comments => fetchCommentsSuccess({comments})),
      catchError( () => of(fetchCommentsFailure({error: 'Error!'})))
    ))
  ))

  createComment = createEffect(() => this.actions.pipe(
    ofType(createCommentRequest),
    mergeMap(comment => this.commentService.createComment(comment.comment).pipe(
      map( () => createCommentSuccess()),
      catchError( () => of(createCommentFailure({error: 'Error!'})))
    ))
  ))

  constructor(
    private actions: Actions,
    private commentService: CommentService
  ) {
  }
}
