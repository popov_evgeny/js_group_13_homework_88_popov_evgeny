import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  createPostFailure,
  createPostRequest,
  createPostSuccess,
  fetchPostFailure,
  fetchPostRequest,
  fetchPostsFailure,
  fetchPostsRequest,
  fetchPostsSuccess,
  fetchPostSuccess
} from './post.actions';
import { catchError, map, mergeMap, of, tap } from 'rxjs';
import { PostService } from '../services/post.service';
import { Router } from '@angular/router';
import { loginFailure } from './users.actions';
import { HelpersService } from '../services/helpers.service';

@Injectable()

export class PostEffects {
  fetchPosts = createEffect(() => this.actions.pipe(
    ofType(fetchPostsRequest),
    mergeMap(() => this.postService.getPosts().pipe(
      map( posts => fetchPostsSuccess( {posts})),
      catchError( () => of(fetchPostsFailure({error: 'Error!'})))
    ))
  ))

  fetchPost = createEffect(() => this.actions.pipe(
    ofType(fetchPostRequest),
    mergeMap(id => this.postService.getOnePost(id.id).pipe(
      map( post => fetchPostSuccess( {post})),
      catchError( () => of(fetchPostFailure({error: 'Error!'})))
    ))
  ))

  createPost = createEffect(() => this.actions.pipe(
    ofType(createPostRequest),
    mergeMap(post => this.postService.createPost(post.post).pipe(
      map( () => createPostSuccess()),
      tap(() => {
        void this.router.navigate(['/']);
      }),
      this.helpers.catchServerError(createPostFailure)
    ))
  ))

  constructor(
    private actions: Actions,
    private postService: PostService,
    private router: Router,
    private helpers: HelpersService,
  ) {
  }
}
