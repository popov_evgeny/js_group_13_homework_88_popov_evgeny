import { createAction, props } from '@ngrx/store';
import { ApiUser, LoginError, LoginUserData, RegisterError, User } from '../models/user.model';

export const registerRequest = createAction('[Register] Register Request', props<{userData: ApiUser}>());
export const registerSuccess = createAction('[Register] Register Success', props<{user: User}>());
export const registerFailure = createAction('[Register] Register Failure', props<{error: null | RegisterError}>());

export const loginRequest = createAction('[Users] Login Request', props<{userData: LoginUserData}>());
export const loginSuccess = createAction('[Users] Login Success', props<{user: User}>());
export const loginFailure = createAction('[Users] Login Failure', props<{error: null | LoginError}>());


export const logoutUser = createAction('[Users] Logout');
export const logoutRequest = createAction('[Users] Server Logout Request');
