import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, mergeMap, NEVER, tap, withLatestFrom } from 'rxjs';
import {
  loginFailure,
  loginRequest,
  loginSuccess,
  logoutRequest,
  logoutUser,
  registerFailure,
  registerRequest,
  registerSuccess
} from './users.actions';
import { UsersService } from '../services/users.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from './types';
import { HelpersService } from '../services/helpers.service';

@Injectable()

export class UsersEffects {
  constructor(
    private actions: Actions,
    private usersService: UsersService,
    private router: Router,
    private snackbar: MatSnackBar,
    private helpers: HelpersService,
    private store: Store<AppState>
  ) {}

 registerUser = createEffect(() => this.actions.pipe(
    ofType(registerRequest),
    mergeMap(({userData}) => this.usersService.registerUser(userData).pipe(
      map( user => registerSuccess( {user})),
      tap(() => {
        this.helpers.openSnackbar('Register successful');
        void this.router.navigate(['/']);
      }),
      this.helpers.catchServerError(registerFailure)
    ))
 ));

  loginUser = createEffect(() => this.actions.pipe(
    ofType(loginRequest),
    mergeMap(({userData}) => this.usersService.login(userData).pipe(
      map(user => loginSuccess({user})),
      tap(() => {
        this.helpers.openSnackbar('Login successful');
        void this.router.navigate(['/']);
      }),
      this.helpers.catchServerError(loginFailure)
    ))
  ))

  logoutUser = createEffect(() => this.actions.pipe(
    ofType(logoutRequest),
    withLatestFrom(this.store.select(state => state.user.user)),
    mergeMap(([_, user]) => {
      if (user) {
        return this.usersService.logout(user.token).pipe(
          map(() => logoutUser()),
          tap(() => this.helpers.openSnackbar('Logout successful'))
        );
      }
      return NEVER;
    }))
  )
}
