import { CommentState } from './types';
import { createReducer, on } from '@ngrx/store';
import {
  createCommentFailure,
  createCommentRequest, createCommentSuccess,
  fetchCommentsFailure,
  fetchCommentsRequest,
  fetchCommentsSuccess
} from './comment.actions';

const initialState: CommentState = {
  comments: [],
  comment: null,
  fetchLoading: false,
  fetchError: null,
  createLoading: false,
  createError: null
}

export const CommentReducer = createReducer(
  initialState,
  on(fetchCommentsRequest, state => ({...state, fetchLoading: true})),
  on(fetchCommentsSuccess, (state, {comments}) => ({...state, fetchLoading: false, comments})),
  on(fetchCommentsFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),

  on(createCommentRequest, state => ({...state, createLoading: true})),
  on(createCommentSuccess, state => ({...state, createLoading: false})),
  on(createCommentFailure, (state, {error}) => ({...state, createLoading: false, createError: error})),
)
