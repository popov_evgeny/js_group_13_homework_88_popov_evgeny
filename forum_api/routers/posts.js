const express = require('express');
const multer = require('multer');
const {nanoid} = require('nanoid');
const config = require("../config");
const path = require("path");
const Post = require("../models/Post");
const mongoose = require("mongoose");
const auth = require("./middleware/auth");

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req,file,cd) => {
    cd(null, config.uploadPath);
  },
  filename: (req, file, cd) => {
    cd(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

router.get('/', async (req , res) => {
  const posts = await Post.find().populate('user', 'name');
  res.send(posts);
});

router.get('/:id', async (req , res) => {
  if( !mongoose.Types.ObjectId.isValid(req.params.id) ) return false;
  const posts = await Post.findOne({_id: req.params.id}).populate('user', 'name');
  res.send(posts);
});

router.post('/',auth, upload.single('image'), async (req , res, next) => {
  try {
    const postData = {
      title: req.body.title,
      user: req.user._id,
      description: req.body.description,
      datetime: new Date().toISOString(),
      image: null
    }

    if (req.file) {
      postData.image = req.file.filename;
    }

    const post = new Post(postData);

    await post.save();

    return res.send({message: 'Added new post in database'});
  } catch (e) {
    if (e instanceof mongoose.Error.ValidationError) {
      return res.status(400).send(e);
    }
    return next(e);
  }
});


module.exports = router;