const express = require('express');
const Comment = require("../models/Comment");
const mongoose = require("mongoose");
const auth = require("./middleware/auth");

const router = express.Router();

router.get('/:id', async (req , res) => {
  const comments = await Comment.find({post: {_id: req.params.id}}).populate('user', 'name');
  res.send(comments);
});

router.post('/', auth, async (req , res, next) => {
  try {
    const commentData = {
      user: req.user._id,
      post: req.body.post,
      description: req.body.description,
    }

    const comment = new Comment(commentData);

    await comment.save();

    return res.send({message: 'Added new comment in database'});
  } catch (e) {
    if (e instanceof mongoose.Error.ValidationError) {
      return res.status(400).send(e);
    }
    return next(e);
  }
});

module.exports = router;