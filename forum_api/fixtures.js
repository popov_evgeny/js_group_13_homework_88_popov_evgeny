const mongoose = require('mongoose');
const config = require("./config");
const Comment = require("./models/Comment");
const Post = require("./models/Post");
const User = require("./models/User");

const run = async () => {
  await mongoose.connect(config.mongo.db, config.mongo.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [user, user2] = await User.create({
    email: 'test@as.com',
    password: '123',
    name: 'John Doe',
    token: '5enDI2paOqusPavVWOnwB'
  }, {
    email: 'test@ad.com',
    password: '123',
    name: 'Jack Doe',
    token: '5enDI2paOasdPavVWOnwB'
  });

  const [post, post2] = await Post.create({
    title: 'MSI GL66 Gaming Laptop',
    description: '15.6" 144Hz FHD 1080p Display, Intel Core i7-11800H, NVIDIA GeForce RTX 3070, 16GB, 512GB SSD, Win10, Black (11UGK-001)',
    image: '',
    user: user,
    datetime: '7241719241929'
  }, {
    title: 'Беспроводные наушники Sony WH-1000XM4',
    description: 'Диапазон частот: 4 - 40000 Гц. Диаметр мембраны: 40 мм. Длина кабеля: 1.2 м. Время работы в режиме прослушивания: до 38 ч. Время полного заряда: около 3 ч. Интерфейсы: Bluetooth 5.0, NFC Проводное соединение: AUX 3,5 мм, USB-C Кодеки: AAC, LDAC, SBC',
    image: '',
    user: user2,
    datetime: '1268127098104'
  });

  const [comment, comment2] = await Comment.create({
    user: user,
    post: post,
    description: 'Projectors',
  }, {
    user: user2,
    post: post2,
    description: 'Laptops',
  });

  await mongoose.connection.close();
};

run().catch(e => console.error(e));