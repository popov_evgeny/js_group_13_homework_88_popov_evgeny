const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PostSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  description: {
    type: String,
    validate: {
      validator: function () {
        if((!this.isModified('description') && this.image)) return true;
        return (!(!this.description && !this.image));

      },
      message: 'Enter image or description!'
    }
  },
  image: {
    type: String,
    validate: {
      validator: function () {
        if ((!this.isModified('image') && this.description)) return true;
        return !(!this.image && !this.description);

      },
      message: 'Enter image or description!'
    },
  },
  datetime: {
    type: String,
    required: true
  }
});

const Post = mongoose.model('Post', PostSchema);

module.exports = Post;